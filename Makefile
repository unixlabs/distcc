all: build tag push
version := $(shell cat version)
build:
	docker build --no-cache -t distcc .
tag:
	docker tag distcc registry.gitlab.com/unixlabs/distcc:${version}
	docker tag distcc registry.gitlab.com/unixlabs/distcc:latest
push:
	docker push registry.gitlab.com/unixlabs/distcc:${version}
	docker push registry.gitlab.com/unixlabs/distcc:latest
