FROM ubuntu
RUN apt update && apt -y install distcc build-essential clang
ENTRYPOINT ["distccd", "--daemon", "--allow", "192.168.178.0/24", "--no-detach", "--log-stderr", "-j", "24"]
